package com.atlassian.soy.test;

import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;

import java.util.Collections;
import java.util.Set;

public class SanityFunction implements SoyServerFunction<String>, SoyClientFunction {
    private static final String MY_NAME_IS = "Slim Shady";

    @Override
    public String apply(Object... args) {
        return MY_NAME_IS;
    }

    @Override
    public JsExpression generate(JsExpression... args) {
        return new JsExpression(MY_NAME_IS);
    }

    @Override
    public String getName() {
        return "myNameIs";
    }

    @Override
    public Set<Integer> validArgSizes() {
        return Collections.singleton(0);
    }
}
