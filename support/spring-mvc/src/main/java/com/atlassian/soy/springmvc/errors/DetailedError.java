package com.atlassian.soy.springmvc.errors;

import com.google.common.collect.ImmutableList;

import java.util.List;

/**
 * Simple error wrapping class.
 *
 * @since 2.3
 */
public class DetailedError {

    private final List<String> details;
    private final String message;

    public DetailedError(String message, Iterable<String> details) {
        this.details = ImmutableList.copyOf(details);
        this.message = message;
    }

    public Iterable<String> getDetails() {
        return details;
    }

    public String getMessage() {
        return message;
    }
}
