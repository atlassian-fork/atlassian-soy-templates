package com.atlassian.soy.cli;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 * Finds files matching a glob pattern
 */
final class FileFinder {
    static List<RelativePath> findFiles(final PathGlob pathGlob) throws IOException {
        final PathMatcher includeMatcher = FileSystems.getDefault().getPathMatcher("glob:" + pathGlob.getInclude());
        final PathMatcher excludeMatcher = pathGlob.getExclude() == null ? null : FileSystems.getDefault().getPathMatcher("glob:" + pathGlob.getExclude());
        final List<RelativePath> matchedPaths = new ArrayList<>();
        final Path basePath = FileSystems.getDefault().getPath(pathGlob.getBaseDirectory());

        Files.walkFileTree(basePath, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                final Path relativeFile = basePath.relativize(file);
                if (includeMatcher.matches(relativeFile)) {
                    if (excludeMatcher == null || !excludeMatcher.matches(relativeFile)) {
                        matchedPaths.add(new RelativePath(file.toFile(), relativeFile.toString()));
                    }
                }
                return super.visitFile(file, attrs);
            }
        });
        return matchedPaths;
    }

}
