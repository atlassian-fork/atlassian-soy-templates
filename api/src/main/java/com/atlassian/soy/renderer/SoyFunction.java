package com.atlassian.soy.renderer;

import com.atlassian.annotations.PublicSpi;

import java.util.Set;

/**
 * super-interface for all soy functions.
 * <p>
 * Don't implement directly. Implement {@link SoyServerFunction} and/or {@link SoyClientFunction}
 *
 * @since 2.0
 */
@PublicSpi
public interface SoyFunction {
    /**
     * @return the name of the function as a String
     */
    String getName();

    /**
     * @return a set containing the valid argument lengths which this function accepts.
     */
    Set<Integer> validArgSizes();
}
