package com.atlassian.soy.renderer;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.atlassian.webresource.api.prebake.DimensionAwareTransformerUrlBuilder;
import com.atlassian.webresource.api.prebake.Dimensions;

/**
 * A specific kinf of {@link SoyClientFunction} that is url contributing and dimension aware,
 * so resources executing this function can be pre-baked using provided dimensions
 * ({@code com.atlassian.webresource.api.prebake.Dimensions}), i.e., states it can assume.
 *
 * @since 4.3.0
 */
@TenantAware(TenancyScope.TENANTLESS)
public interface StatefulSoyClientFunction extends DimensionAwareTransformerUrlBuilder, SoyClientFunction {
    Dimensions computeDimensions();
}
