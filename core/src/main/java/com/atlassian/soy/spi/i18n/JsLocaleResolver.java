package com.atlassian.soy.spi.i18n;

import java.util.Locale;

/**
 * SPI for locale resolution for js functions. Implementors should delegate to whatever the target stack supports.
 *
 * @since 2.8
 */
public interface JsLocaleResolver {
    /**
     * @return the locale to use for i18n resolution when compiling javascript functions
     */
    Locale getLocale();
}
