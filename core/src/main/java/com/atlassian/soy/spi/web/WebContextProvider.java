package com.atlassian.soy.spi.web;

import java.util.Locale;

/**
 * SPI for web context data. Implementors should delegate to whatever the target stack supports
 *
 * @since 2.3
 */
public interface WebContextProvider {
    /**
     * @return context path of the target web-app. Used by templates for rendering links within the app.
     */
    String getContextPath();

    /**
     * @return current locale. Used by templates for i18n purposes.
     */
    Locale getLocale();
}
