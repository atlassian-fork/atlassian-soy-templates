package com.atlassian.soy.impl;

import com.atlassian.soy.spi.TemplateSetFactory;

/**
 * An abstract implementation of {@link TemplateSetFactory} which provides a no-op implementation of the
 * {@link #clear()} method
 *
 * @since 3.2
 */
public abstract class AbstractTemplateSetFactory implements TemplateSetFactory {
    protected AbstractTemplateSetFactory() {
    }

    @Override
    public void clear() {
    }
}
