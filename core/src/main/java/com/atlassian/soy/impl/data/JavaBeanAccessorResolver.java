package com.atlassian.soy.impl.data;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;

import java.lang.reflect.Method;
import java.util.Map;

@TenantAware(value= TenancyScope.TENANTLESS, comment="Caches acsessors of the classes, same for all tenants.")
public interface JavaBeanAccessorResolver {

    void clearCaches();

    Map<String, Method> resolveAccessors(Class<?> targetClass);
}
