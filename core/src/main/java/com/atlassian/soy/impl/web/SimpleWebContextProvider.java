package com.atlassian.soy.impl.web;

import com.atlassian.soy.spi.web.WebContextProvider;

import java.util.Locale;

/**
 * A simple implementation of {@link WebContextProvider} which uses the default locale and a constant context path
 *
 * @since 3.2
 */
public class SimpleWebContextProvider implements WebContextProvider {
    private final String contextPath;

    public SimpleWebContextProvider() {
        this("");
    }

    public SimpleWebContextProvider(final String contextPath) {
        this.contextPath = contextPath;
    }

    @Override
    public String getContextPath() {
        return contextPath;
    }

    @Override
    public Locale getLocale() {
        return Locale.getDefault();
    }
}
