package com.atlassian.soy.impl.data;

import com.atlassian.annotations.tenancy.TenancyScope;
import com.atlassian.annotations.tenancy.TenantAware;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.util.Map;

public class CachingJavaBeanAccessorResolver implements JavaBeanAccessorResolver {
    @TenantAware(value=TenancyScope.TENANTLESS, comment="It caches the results of " +
            "IntrospectorJavaBeanAccessorResolver it's the same for all tenants.")
    private final LoadingCache<Class<?>, Map<String, Method>> accessorCache;

    public CachingJavaBeanAccessorResolver() {
        this(new IntrospectorJavaBeanAccessorResolver());
    }

    public CachingJavaBeanAccessorResolver(final JavaBeanAccessorResolver delegate) {
        this.accessorCache = CacheBuilder.newBuilder()
                .build(new CacheLoader<Class<?>, Map<String, Method>>() {
                    @Override
                    public Map<String, Method> load(@Nonnull Class<?> targetClass) throws Exception {
                        return delegate.resolveAccessors(targetClass);
                    }
                });
    }

    public void clearCaches() {
        accessorCache.invalidateAll();
    }

    @Override
    public Map<String, Method> resolveAccessors(final Class<?> targetClass) {
        return accessorCache.getUnchecked(targetClass);
    }

}
