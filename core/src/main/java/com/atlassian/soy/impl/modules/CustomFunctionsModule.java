package com.atlassian.soy.impl.modules;

import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.atlassian.soy.spi.functions.SoyFunctionSupplier;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.Multibinder;
import com.google.template.soy.shared.restricted.SoyFunction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Bind the functions supplied into the guice context
 */
class CustomFunctionsModule extends AbstractModule {
    // This is a bit shit, but it has a unit test which will fail if new
    // soy functions are added
    @VisibleForTesting
    static final Set<String> BUILT_IN_FUNCTION_NAMES = ImmutableSet.of(
            "augmentMap",
            "ceiling",
            "bidiEndEdge",
            "bidiDirAttr",
            "bidiGlobalDir",
            "bidiMark",
            "bidiMarkAfter",
            "bidiStartEdge",
            "bidiTextDir",
            "floor",
            "isNonnull",
            "keys",
            "length",
            "max",
            "min",
            "randomInt",
            "round",
            "strContains",
            "strIndexOf",
            "strLen",
            "strSub"
    );

    private static final Logger log = LoggerFactory.getLogger(CustomFunctionsModule.class);

    private final SoyFunctionSupplier soyFunctionSupplier;

    public CustomFunctionsModule(SoyFunctionSupplier soyFunctionSupplier) {
        this.soyFunctionSupplier = soyFunctionSupplier;
    }

    @Override
    public void configure() {
        Map<String, SoyFunctionHolder> holders = new HashMap<String, SoyFunctionHolder>();
        for (com.atlassian.soy.renderer.SoyFunction function : soyFunctionSupplier.get()) {
            String name = function.getName();
            if (isProvidedFunctionName(name)) {
                log.info("Ignoring {} as it is attempting to register with name {} but that soy function is already built-in", function.getClass(), name);
                continue;
            }
            SoyFunctionHolder holder = holders.get(name);
            if (holder == null) {
                holder = new SoyFunctionHolder();
                holders.put(name, holder);
            }
            if (!holder.offer(function)) {
                log.info("Ignoring {} as as there is already a function with name {} registered", function.getClass(), name);
            }
        }
        Multibinder<SoyFunction> binder = Multibinder.newSetBinder(binder(), SoyFunction.class);
        for (SoyFunctionHolder holder : holders.values()) {
            binder.addBinding().toInstance(holder.createAdaptor());
        }
    }

    @VisibleForTesting
    static boolean isProvidedFunctionName(String name) {
        return CoreFunctionsModule.CORE_FUNCTION_NAMES.contains(name) ||
                BUILT_IN_FUNCTION_NAMES.contains(name);
    }

    static class SoyFunctionHolder {
        private SoyServerFunction<?> serverFunction;
        private SoyClientFunction clientFunction;

        public boolean offer(com.atlassian.soy.renderer.SoyFunction function) {
            boolean accepted = false;
            if (function instanceof SoyServerFunction && serverFunction == null) {
                serverFunction = (SoyServerFunction<?>) function;
                accepted = true;
            }
            if (function instanceof SoyClientFunction && clientFunction == null) {
                clientFunction = (SoyClientFunction) function;
                accepted = true;
            }
            return accepted;
        }

        public SoyFunction createAdaptor() {
            if (serverFunction != null) {
                if (clientFunction != null) {
                    return new CompositeFunctionAdaptor(serverFunction, clientFunction);
                } else {
                    return new SoyJavaFunctionAdapter(serverFunction);
                }
            } else if (clientFunction != null) {
                return new SoyJsSrcFunctionAdapter(clientFunction);
            } else {
                throw new IllegalStateException("No soy function offered to holder");
            }
        }
    }

}
