package com.atlassian.soy.impl.modules;

import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.base.Objects;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.SoyValueHelper;
import com.google.template.soy.jssrc.restricted.JsExpr;
import com.google.template.soy.jssrc.restricted.SoyJsSrcFunction;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import javax.inject.Inject;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Preconditions.checkArgument;

class CompositeFunctionAdaptor implements SoyJsSrcFunction, SoyJavaFunction {
    private final SoyJavaFunctionAdapter serverAdaptor;
    private final SoyJsSrcFunctionAdapter clientAdaptor;

    public CompositeFunctionAdaptor(SoyServerFunction<?> serverFunction, SoyClientFunction clientFunction) {
        checkArgument(serverFunction == clientFunction ||
                        (Objects.equal(serverFunction.validArgSizes(), clientFunction.validArgSizes()) &&
                                Objects.equal(serverFunction.getName(), clientFunction.getName())),
                "the supplied soy functions are not compatible with each other");
        this.serverAdaptor = new SoyJavaFunctionAdapter(serverFunction);
        this.clientAdaptor = new SoyJsSrcFunctionAdapter(clientFunction);
    }

    @Override
    public JsExpr computeForJsSrc(List<JsExpr> args) {
        return clientAdaptor.computeForJsSrc(args);
    }

    @Override
    public SoyValue computeForJava(List<SoyValue> args) {
        return serverAdaptor.computeForJava(args);
    }

    @Override
    public String getName() {
        return serverAdaptor.getName(); // Is doesn't matter which one is called
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return serverAdaptor.getValidArgsSizes(); // Is doesn't matter which one is called
    }

    @Inject
    void setConverter(SoyValueHelper converter) {
        serverAdaptor.setConverter(converter);
    }

}
