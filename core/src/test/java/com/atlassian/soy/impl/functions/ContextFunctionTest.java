package com.atlassian.soy.impl.functions;

import com.atlassian.soy.spi.web.WebContextProvider;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.jssrc.restricted.JsExpr;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class ContextFunctionTest {
    private static final List<JsExpr> EMPTY_JS_ARGS = Collections.emptyList();
    private static final List<SoyValue> EMPTY_TOFU_ARGS = Collections.emptyList();

    private ContextFunction toTest;

    @Mock
    private WebContextProvider webContextProvider;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        toTest = new ContextFunction(webContextProvider, false);
        when(webContextProvider.getContextPath()).thenReturn(null);
    }

    @Test
    public void baseUrlIsEmpty() {
        when(webContextProvider.getContextPath()).thenReturn("");
        assertEquals("\"\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("", toTest.computeForJava(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlIsJustSlash() {
        when(webContextProvider.getContextPath()).thenReturn("/");
        assertEquals("\"\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("", toTest.computeForJava(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlHasNoTrailingSlash() {
        when(webContextProvider.getContextPath()).thenReturn("foo");
        assertEquals("\"foo\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("foo", toTest.computeForJava(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlHasTrailingSlash() {
        when(webContextProvider.getContextPath()).thenReturn("foo/");
        assertEquals("\"foo\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("foo", toTest.computeForJava(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void baseUrlHasLeadingAndTrailingSlashes() {
        when(webContextProvider.getContextPath()).thenReturn("/foo/");
        assertEquals("\"/foo\"", toTest.computeForJsSrc(EMPTY_JS_ARGS).getText());
        assertEquals("/foo", toTest.computeForJava(EMPTY_TOFU_ARGS).stringValue());
    }

    @Test
    public void updatedBaseUrlIsGenerated() {
        when(webContextProvider.getContextPath()).thenReturn("foo", "bar");
        JsExpr jsResult1 = toTest.computeForJsSrc(EMPTY_JS_ARGS);
        JsExpr jsResult2 = toTest.computeForJsSrc(EMPTY_JS_ARGS);
        assertEquals("\"foo\"", jsResult1.getText());
        assertEquals("\"bar\"", jsResult2.getText());
    }

    @Test
    public void testAjsContextPathUsedWhenConfigured() throws Exception {
        toTest = new ContextFunction(webContextProvider, true);
        JsExpr result = toTest.computeForJsSrc(EMPTY_JS_ARGS);
        assertEquals("AJS.contextPath()", result.getText());
    }
}
