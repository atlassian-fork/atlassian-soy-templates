package com.atlassian.soy.impl;

import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginModuleDisabledEvent;
import com.atlassian.plugin.event.events.PluginModuleEnabledEvent;

public class SoyPluginListenerCacheClearer {
    private final SoyManager soyManager;
    private final PluginEventManager pluginEventManager;

    public SoyPluginListenerCacheClearer(SoyManager soyManager, PluginEventManager pluginEventManager) {
        this.soyManager = soyManager;
        this.pluginEventManager = pluginEventManager;
    }

    public void registerEventListeners() {
        pluginEventManager.register(this);
    }

    public void unregisterEventListeners() {
        pluginEventManager.unregister(this);
    }

    @SuppressWarnings("UnusedDeclaration")
    @PluginEventListener
    public void pluginModuleEnabled(PluginModuleEnabledEvent event) {
        soyManager.clearCaches(null);
    }

    @SuppressWarnings("UnusedDeclaration")
    @PluginEventListener
    public void pluginModuleDisabled(PluginModuleDisabledEvent event) {
        soyManager.clearCaches(null);
    }

}
