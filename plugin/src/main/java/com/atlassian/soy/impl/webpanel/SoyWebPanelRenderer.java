package com.atlassian.soy.impl.webpanel;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.web.renderer.RendererException;
import com.atlassian.plugin.web.renderer.WebPanelRenderer;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import org.apache.commons.lang3.NotImplementedException;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

/**
 * This class was originally taken from the boys in Stash.  By placing it into the main Soy plugin, all products and
 * their developers get to enjoy soy based WebPanel goodness.
 * <p>
 * It is put into play via <renderer-type class="com.atlassian.soy.impl.webpanel.SoyWebPanelRenderer" />
 */
public class SoyWebPanelRenderer implements WebPanelRenderer {
    private final SoyTemplateRenderer soyTemplateRenderer;

    public SoyWebPanelRenderer(SoyTemplateRenderer soyTemplateRenderer) {
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    public String getResourceType() {
        return "soy";
    }

    @Override
    public void render(String templateAddress, Plugin plugin, Map<String, Object> context, Writer writer)
            throws RendererException, IOException {
        try {
            TemplateAddressing.Address address = TemplateAddressing.parseTemplateAddress(templateAddress, plugin.getKey());
            soyTemplateRenderer.render(writer, address.getCompleteKey().toString(), address.getTemplateName(), context);
        } catch (SoyException e) {
            throw new RendererException(e.getMessage(), e);
        }
    }


    @Override
    public String renderFragment(String fragment, Plugin plugin, Map<String, Object> context) throws RendererException {
        throw new NotImplementedException("Not implemented for SoyWebPanelRenderer");
    }

    @Override
    public void renderFragment(Writer writer, String fragment, Plugin plugin, Map<String, Object> context)
            throws RendererException, IOException {
        throw new NotImplementedException("Not implemented for SoyWebPanelRenderer");
    }
}