package com.atlassian.soy.impl.functions;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.google.inject.Singleton;
import com.google.template.soy.base.SoySyntaxException;
import com.google.template.soy.data.SoyData;
import com.google.template.soy.data.SoyValue;
import com.google.template.soy.data.restricted.StringData;
import com.google.template.soy.shared.restricted.SoyJavaFunction;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Singleton
public class RequireResourceFunction implements SoyJavaFunction {

    private static final Set<Integer> ARGS_SIZE = Collections.singleton(1);

    private final WebResourceManager webResourceManager;

    @Inject
    public RequireResourceFunction(WebResourceManager webResourceManager) {
        this.webResourceManager = webResourceManager;
    }

    @Override
    public SoyData computeForJava(List<SoyValue> args) {
        final SoyValue data = args.get(0);
        if (!(data instanceof StringData)) {
            throw SoySyntaxException.createWithoutMetaInfo("Argument to " + getName() + "() is not a literal string");
        }
        String moduleKey = data.stringValue();
        webResourceManager.requireResource(moduleKey);
        return StringData.EMPTY_STRING;
    }

    @Override
    public String getName() {
        return "webResourceManager_requireResource";
    }

    @Override
    public Set<Integer> getValidArgsSizes() {
        return ARGS_SIZE;
    }
}
