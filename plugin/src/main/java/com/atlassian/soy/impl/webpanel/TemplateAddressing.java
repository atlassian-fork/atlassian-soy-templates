package com.atlassian.soy.impl.webpanel;

import com.atlassian.plugin.ModuleCompleteKey;
import com.atlassian.soy.renderer.SoyException;
import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBefore;

/**
 * Encapsulates the code that takes a Soy template address and returns the valid parts
 */
class TemplateAddressing {

    static class Address {
        private final ModuleCompleteKey completeKey;
        private final String templateName;

        Address(final ModuleCompleteKey completeKey, final String templateName) {
            this.completeKey = completeKey;
            this.templateName = templateName;
        }

        public ModuleCompleteKey getCompleteKey() {
            return completeKey;
        }

        public String getTemplateName() {
            return templateName;
        }
    }

    /**
     * Parses the passed in address and splits it into completeModuleKey and Soy template address
     *
     * @param templateAddress  the address that the developer has provided
     * @param callingPluginKey the key of the calling plugin.  This can be used for defaulting reasons
     * @return completeModuleKey and Soy template address
     */
    public static Address parseTemplateAddress(final String templateAddress, final String callingPluginKey)
            throws SoyException {
        return parseAtlassianTemplateAddress(templateAddress, callingPluginKey);
    }

    /*
     * Must be in form pluginKey:moduleKey/templateName or :moduleKey/templateName
     */
    private static Address parseAtlassianTemplateAddress(final String templateAddress, final String callingPluginKey)
            throws SoyException {
        String completeKey = substringBefore(templateAddress, "/");
        String templateName = substringAfter(templateAddress, "/");
        if (isEmpty(completeKey) || isEmpty(templateName)) {
            throw badTemplateName(templateAddress);
        }

        if (StringUtils.countMatches(completeKey, ":") != 1) {
            throw badTemplateName(templateAddress);
        }
        String pluginKey = substringBefore(completeKey, ":");
        String moduleKey = substringAfter(completeKey, ":");
        // we allow defaulting
        if (isEmpty(pluginKey) || ".".equals(pluginKey)) {
            pluginKey = callingPluginKey;
        }
        if (isEmpty(pluginKey) || isEmpty(moduleKey)) {
            throw badTemplateName(templateAddress);
        }
        return new Address(new ModuleCompleteKey(pluginKey, moduleKey), templateName);
    }

    private static SoyException badTemplateName(final String templateAddress) {
        return new SoyException(String.format("Template name must be in the form 'pluginKey:moduleKey/templateAddress' - '%s'", templateAddress));
    }

}
